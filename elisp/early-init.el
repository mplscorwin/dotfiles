;;; early-init.el --- early bird  -*- no-byte-compile: t -*-
;;(message "[%s] top of ~/.emacs.d/early-init.el" (current-time-string))
;;(setq load-prefer-newer t)
;;(add-to-list 'load-path "c:/users/corwi/.emacs.d/elpa/auto-compile-20201122.1157/")
;;(add-to-list 'load-path "/path/to/auto-compile")
;; (require 'auto-compile)
;; (auto-compile-on-load-mode)
;; (auto-compile-on-save-mode)

(when (version< emacs-version "28.0")
  (if (version< emacs-version "27.0")
      (setq package-user-dir "c:/Users/corwi/.emacs.d/elpa-26")
    (setq package-user-dir "c:/Users/corwi/.emacs.d/elpa-27")))

;;(message "[%s] end of ~/.emacs.d/early-init.el" (current-time-string))
;;; early-init.el ends here
