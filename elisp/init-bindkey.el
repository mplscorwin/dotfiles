;;; init-bindkey.el --- setup keybindings for Emacs  -*- lexical-binding: t; -*-

;; Copyright (C) 2020  Corwin Brust

;; Author: Corwin Brust <corwin@bru.st>
;; Keywords:

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;;

;;; Code:

(global-set-key (kbd "C-x M-t") #'cleanup-region)
(global-set-key (kbd "C-c n") #'cleanup-buffer)
(global-set-key (kbd "C-a") #'my/smarter-move-beginning-of-line)
(global-set-key (kbd "RET") #'newline-and-indent)
(global-set-key (kbd "C-;") #'comment-or-uncomment-region)
;; covered by the use-package
;;(global-set-key (kbd "M-/") #'hippie-expand)
(global-set-key (kbd "C-+") #'text-scale-increase)
(global-set-key (kbd "C--") #'text-scale-decrease)
(global-set-key (kbd "C-c C-k") #'compile)
(global-set-key (kbd "C-x g") #'magit-status)
(global-set-key (kbd "M-Q") #'unfill-paragraph)
(global-set-key (kbd "M-C-q") #'my:refill-paragraph)
(global-set-key (kbd "M-C-#") #'cycle-display-line-numbers)
(global-set-key (kbd "C-c a") #'org-agenda)
(global-set-key (kbd "C-x M-<up>") #'windmove-up)
(global-set-key (kbd "C-x M-<down>") #'windmove-down)
(global-set-key (kbd "C-x M-<left>") #'windmove-left)
(global-set-key (kbd "C-x M-<right>") #'windmove-right)
(global-set-key (kbd "C-x M-i") #'my:load-init-library)

(provide 'init-bindkey)
;;; init-bindkey.el ends here
