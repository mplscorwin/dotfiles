;;; init-company.el --- setup company completion for Emacs  -*- lexical-binding: t; -*-

;; Copyright (C) 2020

;; Author:  <corwi@AVALON>
;; Keywords:

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;;

;;; Code:

(use-package company
  :ensure t
  :config
  (add-to-list 'company-backends 'company-math-symbols-unicode)
  (add-to-list 'company-backends 'company-elisp)
  (add-to-list 'company-backends 'company-emoji)
  ;; extra documentation while you wait
  (use-package company-quickhelp :ensure t :config
    (company-quickhelp-mode))
  ;; M-<number> to select a completion
  (setq company-show-numbers t)
  (setq company-tooltip-align-annotations t)
  ;; invert direction when popup is near bottom of window
  ;; TODO: can we also invert the keymap here somehow?
  (setq company-tooltip-flip-when-above t)
  (setq company-tooltip-align-annotations t)
  (global-company-mode)) ;; use everywhere

;;;; beautify 😸
(use-package company-box
  :hook (company-mode . company-box-mode))

;; perl backend
;; (eval-after-load 'cperl-mode
;;   (add-to-list 'company-backends 'company-plsense))

;; (eval-after-load 'erc
;;   (use-package company-emoji :ensure t :config
;;     (add-to-list 'company-backends 'company-emoji)))

(provide 'init-company)
;;; init-company.el ends here
