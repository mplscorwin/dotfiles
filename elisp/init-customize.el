;;; init-customize.el --- save emacs customizations here  -*- lexical-binding: t; -*-

;; Copyright (C) 2020  Corwin Brust

;; Author: Corwin Brust <corwin@bru.st>
;; Keywords:

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;; moved from .emacs 2020-02-16

;;; Code:

(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(package-selected-packages '(diminish auto-compile use-package)))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )

;; other packages
   ;; '(hide-lines keycast lsp-ui auto-compile valign org-tree-slide org-present org-superstar spaceline-all-the-icons spaceline clippy seq which-key-posframe mmm-mode emojify-logos company-org-roam company-php company-shell company-web dashboard-hackernews dashboard-ls dashboard-project-status date-at-point dashboard elcord password-store material-theme xr kaolin-themes erc-scrolltoplace debbugs modus-vivendi-theme modus-operandi-theme yarn-mode yaml-mode writegood-mode which-key webpaste web-mode-edit-element use-ttf use-package treemacs-projectile treemacs-magit treemacs-icons-dired tramp tide smex smart-hungry-delete rjsx-mode rainbow-mode rainbow-delimiters quelpa projectile-git-autofetch prettier-js powershell powerline plsense-direx pdf-tools paredit org-tanglesync org-caldav org-bullets org-beautify-theme org-agenda-property ob-typescript nyan-mode mode-icons minions lsp-treemacs latex-unicode-math-mode latex-preview-pane latex-math-preview lab-themes ix idle-org-agenda ibuffer-projectile htmlize helpful helm hack-time-mode graphviz-dot-mode git-auto-commit-mode gist gif-screencast fullframe forge flycheck-package floobits feature-mode ercn erc-yt erc-youtube erc-view-log erc-twitch erc-tweet erc-status-sidebar erc-image erc-hl-nicks erc-colorize emojify elpy elisp-format elisp-demos eldoc-overlay eldoc-box el2org edit-server dracula-theme doom-themes doom-modeline dired-git diminish-buffer diminish delight darktooth-theme cyberpunk-theme cyberpunk-2019-theme curl-to-elisp company-try-hard company-restclient company-quickhelp company-prescient company-posframe company-plsense company-native-complete company-math company-fuzzy company-emoji company-dict company-ctags company-box color-theme-buffer-local color-identifiers-mode code-archive cask buttercup bug-hunter bufler browse-kill-ring borland-blue-theme bbdb autopair auctex ascii-art-to-unicode all-the-icons-ibuffer all-the-icons-gnus all-the-icons-dired ac-slime)

(provide 'init-customize)
;;; init-customize.el ends here
