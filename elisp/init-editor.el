;;; init-editor.el --- configure Emacs for general editing use  -*- lexical-binding: t; -*-

;; Copyright (C) 2020  Corwin Brust

;; Author: Corwin Brust <corwin@bru.st>
;; Keywords:

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;; global configuration ment for Emacs startup sequence

;;; Code:

(eval-when-compile (require 'use-package))

(setq user-full-name "Corwin Brust")
(setq user-mail-address "corwin@bru.st")

;; shhh
(defalias 'yes-or-no-p 'y-or-n-p)

;; disable boilerplate UX gadgets
(scroll-bar-mode -1)
(tool-bar-mode -1)
(menu-bar-mode -1)

;; unfuckify
(setq echo-keystrokes 0.1
      use-dialog-box nil
      visible-bell t
      delete-by-moving-to-trash t)
(show-paren-mode t)
(prefer-coding-system 'utf-8)

;; clipboard
(delete-selection-mode t)
(transient-mark-mode t)
(setq x-select-enable-clipboard t)

;; display line endings
(setq-default show-trailing-whitespace t)
(setq-default indicate-empty-lines t)
(when (not indicate-empty-lines)
  (toggle-indicate-empty-lines))

;;;;;;;;;;
;; line setup from https://github.com/mrvdb/emacs-config
(setq-default truncate-lines nil)
;; Similar to mail messages, use vertical bar for wrapped paragaphs
(setq visual-line-fringe-indicators '(vertical-bar nil))

;; For all text modes use visual-line-mode
(add-hook 'text-mode-hook 'visual-line-mode)

;; clobber tab related default
(setq tab-width 2
      indent-tabs-mode nil)

(setq column-number-mode t)
(require 'autopair nil t) ;; local source (EW?)
(use-package rainbow-delimiters :ensure t)

;; TODO move this to init-erc?
(setq erc-interpret-mirc-color t)

;; fullframe
(use-package fullframe :ensure t)

(use-package hl-todo :ensure t :config (global-hl-todo-mode))

(use-package powershell :ensure t)

;; help learning the keys
(use-package which-key
  :ensure t
  :defer 0.5
  :delight
  :config (which-key-mode))

;;; playing with smart-hungry delete
(use-package smart-hungry-delete
  :ensure t
  :bind (("<backspace>" . smart-hungry-delete-backward-char)
		 ("C-d" . smart-hungry-delete-forward-char))
  :defer nil ;; dont defer so we can add our functions to hooks
  :config (smart-hungry-delete-add-default-hooks))

(use-package emojify
  :ensure t
  ;; :after erc
  :defer 15
  :config
  (global-emojify-mode)
  (global-emojify-mode-line-mode -1))

;; whitespace on-save cleanup
(use-package ws-butler
  :ensure nil ;; ALERT: local sources!
  :diminish
  :config
  (add-hook 'prog-mode-hook 'ws-butler-mode)
  (add-hook 'text-mode-hook 'ws-butler-mode)
  ;;(ws-butler-global-mode)
  )

;; Save places in buffers between sessions
(use-package saveplace
  :ensure nil ;builtin
  :init
  (setq-default save-place-mode t))

;; local sources.  I think...
(use-package fullframe :ensure nil)

;; PDF tools - https://github.com/mrvdb/emacs-config
(use-package pdf-tools :ensure t
  :after (pdf-annot fullframe)
  :magic ("%PDF" . pdf-view-mode)
  :bind (:map pdf-view-mode-map
              ("h"   . 'pdf-annot-add-highlight-markup-annotation)
              ("t"   . 'pdf-annot-add-text-annotation)
              ("D"   . 'pdf-annot-delete)
              ("C-s" . 'isearch-forward)
              ;; ("m"   . 'mrb/mailfile)
              :map pdf-annot-edit-contents-minor-mode-map
              ("<return>"   . 'pdf-annot-edit-contents-commit)
              ("<S-return>" .  'newline))

  :config
  ;; Some settings from http://pragmaticemacs.com/emacs/even-more-pdf-tools-tweaks/
  (when (featurep 'fulframe) (fullframe pdf-view-mode quit-window))
  (setq-default pdf-view-display-size 'fit-page)  ;;scale to fit page by default
  (setq pdf-annot-activate-created-annotations t) ;; automatically annotate highlights
  (setq pdf-view-resize-factor 1.1)	; more fine-grained zooming
  (add-hook 'pdf-view-mode-hook (lambda () (cua-mode 0))) ;; turn off cua so copy works
  (pdf-tools-install :no-query))		  ;; no-query auto builds epfinfo when needed

;; spell check with hunspell under windows
;; https://lists.gnu.org/archive/html/help-gnu-emacs/2014-04/msg00030.html
;; DONE: should be predicated w64; else use system defaults
(use-package ispell
  ;; :after flyspell
  ;; :if (memq window-system '(w32 pc))
  :init
  (when (eq system-type 'windows-nt)
    (add-to-list 'exec-path "D:/hunspell/bin/")
    (setq ispell-program-name
	  (locate-file "hunspell" exec-path exec-suffixes 'file-executable-p))
    (setq ispell-local-dictionary-alist
	  '((nil
	     "[[:alpha:]]" "[^[:alpha:]]" "[']" t
	     ("-d" "en_US" "-p" "D:\\hunspell\\share\\hunspell\\personal.en")
	     nil iso-8859-1)
	    ("american"
	     "[[:alpha:]]" "[^[:alpha:]]" "[']" t
	     ("-d" "en_US" "-p" "D:\\hunspell\\share\\hunspell\\personal.en")
	     nil iso-8859-1))))
  (setq-default ispell-list-command "list")
  (setq flyspell-issue-welcome-flag nil))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; completion setup

;; basic completion with hippie-expand

;; https://github.com/asummers/.emacs.d/blob/master/init.el
(use-package hippie-exp
  ;;:ensure t
  :bind
  ("M-/" . hippie-expand)
  :config
  (setq hippie-expand-try-functions-list
	'(try-expand-dabbrev
	  try-expand-dabbrev-all-buffers
	  try-expand-dabbrev-from-kill
	  try-complete-file-name-partially
	  try-complete-file-name
	  try-expand-all-abbrevs
	  try-expand-list
	  try-expand-line)))


;; "smex is a necessity. It provides history and searching on top of M-x."
;; (setq smex-save-file (expand-file-name ".smex-items" user-emacs-directory))
;; (smex-initialize)
;; (global-set-key (kbd "M-x") 'smex)
;; (global-set-key (kbd "M-X") 'smex-major-mode-commands)

;; ido filesystem nav
(ido-mode t)
(setq ido-enable-flex-matching t
      ido-use-virtual-buffers t)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; window and frame setup and movement

;; undo/redo window arrangment commands with C-c <left-or-right>

(require 'winner)
(winner-mode)

;; zip window to window with C-M-<arrow>
;; binds are over in init-keybinds.el
(require 'windmove)
;; change frame when zipping past edgeward window
;; ALERT local source: https://www.emacswiki.org/emacs/framemove.el
(require 'framemove nil t) ;; don't error when a local source isn't
(setq framemove-hook-into-windmove t)

;; end windown and frame setup and movement
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;; linting with flycheck
;; https://gist.github.com/daniel-vu/25826682d9af2a201a150aa3c4a4321a
;; (use-package flycheck
;;   :ensure t
;;   :defer 2
;;   :delight
;;   :init (global-flycheck-mode)
;;   :custom
;;   (flycheck-display-errors-delay .3)
;;   :config
;;   ;;(add-hook 'typescript-mode-hook 'flycheck-mode)
;;   )

(provide 'init-editor)
;;; init-editor.el ends here
