;;; init-erc-types.el --- hide types the hard way (with an hack to erc.el)  -*- lexical-binding: t; -*-

;; Copyright (C) 2020  Corwin Brust

;; Author: Corwin Brust <corwin@bru.st>
;; Keywords: IRC, ERC

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;; requires an hack to erc.el
;; diff -u "c:/emacs/share/emacs/27.1/lisp/erc/erc.el" "c:/emacs/share/emacs/27.1/lisp/erc/erc.el~"
;; --- c:/emacs/share/emacs/27.1/lisp/erc/erc.el	2020-12-04 02:56:26.764969500 -0600
;; +++ c:/emacs/share/emacs/27.1/lisp/erc/erc.el~	2020-11-01 02:21:21.007952300 -0600
;; @@ -2741,10 +2741,6 @@
;;  	(when (erc-response.tags parsed)
;;  	  (erc-put-text-property 0 (length string) 'tags (erc-response.tags parsed)
;;  				 string))
;; -	(erc-put-text-property 0 (length string)
;; -			       'erc-type
;; -			       (intern (format "s%s" (erc-response.command parsed)))
;; -			       string)
;;  	(erc-display-line string buffer)))))
;;
;;  (defun erc-message-type-member (position list)

;;; Code:


(defvar my:erc-types
  '((sQUIT :string "QUIT" :documentation "Quit message to channel")
    (sMODE :string "MODE" :documentation "Mode for channel")
    (sJOIN :string "JOIN" :documentation "Join channel")
    (sPART :string "PART" :documentation "Part channel")
    (sKICK :string "KICK" :documentation "Kick from channel")
    (sNICK :string "NICK" :documentation "Nick chat")
    (s301  :string "301"  :documentation "Away notice")
    (s305  :string "305"  :documentation "Return from awayness")
    (s306  :string "306"  :documentation "Set awayness")
    (s324  :string "324"  :documentation "Channel modes on join ")
    (s329  :string "329"  :documentation "Channel creation date")
    (s332  :string "332"  :documentation "Topic notice")
    (s333  :string "333"  :documentation "Who set the topic")
    (s353  :string "353"  :documentation "Names notice"))
  "List of ERC message types for visibility toggle.")

(defvar my:erc-channel-hide-types
  '((t :equal (sPART sJOIN sQUIT s301 sNICK)))
  "Alist mapping channels to messages to hide when joining.

Keys are strings naming channels or the the special entry t,
which provides a default or starting list of message types, as
symbols.  Possible keys include :equal which supplies a list and
causes defaults to be ignored, :and which suppliments the
default list, and :not which filters the list so far.")

(defun my:erc-channel-hide--flatten (defaults args)
  "Return a list of ERC types from DEFAULTS considering ARGS.

For more information on the \"flattening\" logic see
`my:erc-channel-hide-type'."
  (if (and (listp args) (not (keywordp (car args))))
      (remove :IGNORE
	      (append (or (and defaults (seq-copy defaults)) (list :IGNORE))
		      (or (and args (seq-copy args)) (list :IGNORE))))
    (let ((rv (seq-copy defaults))
	  (args (seq-copy args)))
      (while (not (null args))
	;;(message "car:%s cadr:%s rv:%s" (car args) (cadr args) rv)
	(pcase (list (car args) (cadr args))
	  (`(:equal ,(and new-list (guard (listp new-list))))
	   ;; replace the working rv
	   (setq rv (seq-copy new-list)))
	  (`(:and ,(and to-add (guard (not (null to-add)))))
	   (setq rv (seq-uniq
		     (delq nil (append rv (if (listp to-add)
					      (seq-copy to-add)
					    (list to-add)))))))
	  (`(:not ,(and rm-from (guard (not (null rm-from)))))
	   (setq rv (seq-uniq
		     (delq nil (seq-difference rv (if (listp rm-from)
						      rm-from
						    (list rm-from)))))))
	  (`(,(and erc-type (guard (symbolp erc-type))))
	   ;; ZZZ guard for membership in my:erc-channel-hide-types?
	   (setq rv (seq-uniq (delq nil (append rv (list erc-type)))))))
	(if (keywordp (car args))
	    (if (and (< 1 (length args))
		     (nth 2 args)
		     (not (keywordp (nth 2 args))))
		(setq args (list (car args) (cddr args))) ;; leave kw
	      (setq args (cdr-safe (cdr args)))) ;; rm kw & arg
	  (setq args (cdr args))) ;; remove current only
	;;(message "car:%s cadr:%s rv:%s" (car args) (cadr args) rv)
	)
      rv)))

(defun my:erc-channel-hide--unflatten (args)
  "Return ARGS suitable for car of `my:erc-channel-hide-types'.

ARGS is a mixed list of keywords and ERC types.  When no keywords
are included prepend :and.  Otherwise when ARGS does not start
with a keyword prepend :only."
  (if (and args (not (seq-find #'listp args)))
    (if (seq-find #'keywordp args)
	(let ((args (if (keywordp (car args)) args
		      (append (list :equal) args)))
	      this last-list rv)
	  (while (setq this (pop args))
	    (if (not (keywordp this))
		(setq last-list (if (not last-list) (list this)
				  (append last-list (list this))))
	      (setq rv (if (null last-list) (append rv (list this))
			 (append rv (list last-list this))))
	      (setq last-list nil)))
	  ;; tack on the last list, if any
	  (if (null last-list) rv (append rv last-list)))
      ;; no keywords
      (append (list :and) (seq-copy args)))
    args))

(defvar my:erc-channel-hide-ch-hist nil
  "Stores history of channels we changed show-hide for.")
(defvar my:erc-channel-hide-ty-hist nil
  "Stores history of channels we changed show-hide for.")

(defmacro my:erc-channel-hide--read-multi-completed (mess)
  "Extract and read string treating MESS as elisp forms."
  `(read (format "(%s)" (car ,mess))))

(defun my:erc-channel-hide (&optional channel &rest args)
  "Hide messages for CHANNEL per ARGS, a list of ERC types.

When called interactively from an ERC channel buffer prompt for
types to hide in the current buffer.  Otherwise, prompt for CHANNEL.

When called from elisp and CHANNEL is not a string prepend to
ARGS and take CHANNEL from current buffer given it is an ERC
channel.  When ARGS are nill, set `buffer-invisibility-spec' per
`my:erc-channel-hide-types' for the current CHANNEL.

If ERC is inactive signal an error when CHANNEL is nil.  If ERC
is active but CHANNEL's buffer isn't current, switch temporarly.
ARGS may be one or a sequnece of symbols or keywords followed by
symbols or lists of symbols to write to `my:erc-channel-hide-types'."
  (interactive
   (let* ((buffer-list (buffer-list))
	  (channel
	   (if (seq-find #'erc-server-process-alive buffer-list)
	       (completing-read ;; connected; offer attached buffers
		"Channel: "
		(mapcar #'buffer-name buffer-list)
		#'erc-server-process-alive ;;erc-channel-p
		nil
		nil
		'my:erc-channel-hide-ch-hist
		(and (erc-channel-p (current-buffer))
		     (buffer-name)))
	     (completing-read ;; complete from var if not connected
	      "Channel: "
	      (mapcar 'car my:erc-channel-hide-types)
	      nil nil nil
	      'my:erc-channel-hide-ch-hist
	      (and (listp my:erc-channel-hide-ch-hist)
		   (car my:erc-channel-hide-ch-hist)))))
	  (modes
	   (my:erc-channel-hide--unflatten
	    (my:erc-channel-hide--read-multi-completed
	     (completing-read-multiple
	      "Hide types:"
	      (append (list :equal :and :not)
		      (mapcar 'car my:erc-types))
	      nil nil
	      (and channel
		   (replace-regexp-in-string
		    "[(,)]" ", "
		    (mapconcat
		     #'prin1-to-string
		     (or (and nil (erc-channel-p channel)
			      (with-current-buffer channel
				buffer-invisibility-spec))
			 ;; take default from var when not connected
			 (my:erc-channel-hide--flatten
			  (cdr (assoc t my:erc-channel-hide-types))
			  (cdr (assoc channel my:erc-channel-hide-types))))
		     ", ")
		    t t))
	      'my:erc-channel-hide-ty-hist)))))
     (append ;; first, select a channel TODO: do several at once!
      (list channel) modes)))
  (message "starting args:%s" args)
  (let ((args args)
	(buffer-list (buffer-list)))
    (or (and channel
	     (or (equal t channel)
		 (stringp channel)
		 (and (symbolp channel) ;; treat symbol as type for first arg
		      (cl-pushnew channel args)
		      (setq channel nil)))
	     channel)
	(and (setq channel (buffer-name)) ;; use the current buffer's name
	     (erc-channel-p channel)     ;; make sure that's an ERC channel
	     channel)
	(user-error "No channel and an ERC buffer is not current"))

    ;;(message "b4 unflatten args:%s" args)

    ;; handle flat non-interatively supplied arg list
    (unless (listp (nth 1 args))
      (setq args (my:erc-channel-hide--unflatten args)))
    ;;(message "unflattened args:%s" args)

    (let ((erc-connected (seq-find #'erc-server-process-alive buffer-list)))
      (if (and erc-connected (null args))
	  ;; fetch channel or default settings from variable
	  (setq args (or (cdr (assoc channel my:erc-channel-hide-types))
			 (cdr (assoc t my:erc-channel-hide-types))))
	;; save to the variable
	(if-let ((entry (assoc channel my:erc-channel-hide-types)))
	    (progn
	      ;; (message "args: %s unflat:%s"
	      ;; 	       args (my:erc-channel-hide--unflatten args))
	      (setcdr entry (my:erc-channel-hide--unflatten args)))
	  (push (list channel args)
		my:erc-channel-hide-types)))
      ;; update invisibility spec
      (if erc-connected
	  (if (equal t channel)
	      ;; target is default, update all ERC channel buffers
	      (mapc (lambda (buffer)
		      (with-current-buffer buffer
			(setq buffer-invisibility-spec
			      (my:erc-channel-hide--flatten
			       (cdr (assoc t my:erc-channel-hide-types))
			       args))))
		    (seq-filter #'erc-channel-p buffer-list))
	    (if (equal channel (buffer-name))
		;; target is current buffer
		(setq buffer-invisibility-spec
		      (my:erc-channel-hide--flatten
		       (my:erc-channel-hide--flatten
			nil (cdr (assoc t my:erc-channel-hide-types)))
		       args))
	      ;; target is another buffer
	      (when (buffer-live-p channel)
		(with-current-buffer channel
		  (my:erc-channel-hide--flatten
		   (my:erc-channel-hide--flatten
		    nil (cdr (assoc t my:erc-channel-hide-types)))
		   args)))))))))

;;(print (list args (read args) (read (car args)) (seq-map #'read args)))
;; (setq buffer-invisibility-spec '(sPART sJOIN sQUIT s301 sNICK))

;; custom stuff to hide instead of ignore various codes
(defun my:erc-add-invisibility ()
  "Use invisibility spect to hide messages by type.

Requires our hack to create an erc-type text property within
`erc-display-message'."
  (let* ((inhibit-read-only t)
	 (new (get-text-property (- (point-max) 2) 'erc-type))
	 (old (get-text-property (+ (point-min) 2) 'invisible))
	 (inv (if old (if (listp old)
			  (append old (list new))
			(list old new))
		new)))
    (add-text-properties (point-min) (point-max)
			 (list 'invisible inv))))

(add-hook 'erc-insert-post-hook #'my:erc-add-invisibility)

;; given our hacks to erc.el, the above sets up text properties.
;; this plays initial settings from the defcustom to new ERC buffers

(defun my:erc-channel-hide--erc-join-hook (&rest _)
  "Setup `buffer-invisibility-spec' per `my:erc-channel-hide-types'.

The current buffer must be an ERC buffer.  This function is
intended to be run from `erc-join-hook', e.g:
  (add-hook 'erc-join-hook 'my:erc-channel-hide--erc-join-hook)

If no channel specific settings exist, use the defaults (value of the
t entry in `my:erc-channel-hide-types') is used, otherwise
combine channel type list with defaults.

See the command `my:erc-channel-hide' to interactively set
channel specific or default lists.  Start channel specific
settings with :equal to ignore/replace defaults when combining or
:not to supply a list in terms of keys to show instead of to hide
when consolidating with the list of default types."
  (setq-local
   buffer-invisibility-spec
   ;; flatten turns (:equal a b :add c) into '(a b c)
   (my:erc-channel-hide--flatten
    ;; flatten the defaults
    (my:erc-channel-hide--flatten nil (cdr (assoc t my:erc-channel-hide-types)))
    (cdr-safe (assoc (buffer-name) my:erc-channel-hide-types)))))
(add-hook 'erc-join-hook #'my:erc-channel-hide--erc-join-hook)


(provide 'init-erc-types)
;;; init-erc-types.el ends here
