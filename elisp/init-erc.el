;;; init-erc.el --- setup ERC, an Emacs IRC client -*- lexical-binding: t; -*-

;; Copyright (C) 2020  Corwin Brust

;; Author: Corwin Brust <corwin@bru.st>
;; Keywords:

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;;  TODO: play with `erc-bar'  https://www.emacswiki.org/emacs/ErcBar
;;    (overlay-put erc-bar-overlay 'face '(:underline "#2aa198" :style wave))

;;; Code:

(eval-when-compile (require 'cl-macs) (require 'use-package))
;;(eval-when-compile (require 'use-paqckage))

;;(autoload 'erc "erc" "" t)
;; this file isn't auto-loaded so load ERC immediately
(require 'erc)
;;(require 'erc-autojoin)
(require 'erc-join)
(require 'easymenu)

;; so far just one custom ERC specific binding
;; remove blank lines when pasting into ERC mode buffers
(define-key erc-mode-map (kbd "<M-y>") 'yank-unfilled)

(defun erc-cmd-TRACK (&optional target)
  "Start tracking TARGET (or current channel)."
  (interactive)
  (let ((target (or target (buffer-name))))
    (setq erc-track-exclude (remove target erc-track-exclude))))

 (defun erc-cmd-UNTRACK (&optional target)
   "Stop tracking TARGET (or current channel)."
   (interactive)
   (let ((target (or target (buffer-name))))
     (setq erc-track-exclude
	   (append (list target)
		   (remove target erc-track-exclude)))))

;; author's devel work
(let ((load-path
       (append (list (if (eq system-type 'windows-nt)
			 "d:/projects/dotfiles/elisp/site-lisp/erc-hide-line/"
		       "~/projects/erc-hide-line"))
	       load-path)))
  (require 'erc-hide-line)
  (when (bound-and-true-p erc-hl-channels)
    (setcdr (assoc t erc-hl-channels)
	    '(QUIT JOIN PART NICK s301 s305 s306 s324 s329 s333 s353))
    (add-to-list 'erc-hl-channels '("#freenode" :only s353)))
  (erc-hl-enable))
  ;;(add-hook 'erc-after-connect (lambda (_y _y) (erc-hide-line-enable)))

(use-package erc-count-users-mode
  :ensure nil
  :config (add-hook 'erc-mode-hook 'erc-count-users-mode))

;; load available ERC modules in the order given; don't install.
  (when (length
	 (seq-filter
	  (lambda(module)
	    (let ((pkg (intern (concat "erc-" (symbol-name module)))))
	      `(use-package ,pkg :ensure nil :config
		 (add-to-list 'erc-modules module t))))
	  '(match spelling pcomplete log stamp image)))
    (erc-update-modules)) ;; let ERC know if we loaded any

(when  (require 'erc-image nil t)
  (add-to-list 'erc-modules 'image))
(erc-update-modules)

;; handle erc-goodies.el seperatly when available
(use-package erc-goodies :ensure nil
  :config (setq erc-interpret-mirc-color t))

;; first our most basic local settings
(setq erc-prompt (lambda () (concat "[" (buffer-name) "]"))
      erc-autojoin-channels-alist '(("freenode.net"  "#emacs" "#perl"
				     ;;"##typescript" "#httpd"
				     "#nethack"
				     "#dungeon-mode" "#erc" "#org-mode"
				     "#fsf")
				    ;; ("irc.prison.net" "#perl" "#javascript")
				    ("irc.prison.net" "#corwin")
				    ("irc.MagNET.org" "#perl")
				    ("irc.perl.org" "#perl"))
      erc-keywords '("corwin" "@corwin" "brust" ;; fixme: discord bridge
		     "taltos" "dragaera" "zelazny"
		     "dungeon" "burnt-toast")
      erc-prompt (lambda () (concat "[" (buffer-name) "]"))
      erc-hide-list '(
		      ;;"JOIN" "MODE" "NICK" "PART" "QUIT"  ;;"KICK"
		      ;;"301"   ; away notice
		      ;;"305"   ; return from awayness
		      ;;"306"   ; set awayness
		      ;; "324"   ; modes
		      ;; "329"   ; channel creation date
		      ;; "332"   ; topic notice
		      ;; "333"   ; who set the topic
		      ;; "353"   ; Names notice
		      )
      ;; nicely commented IRC message types
      ;; https://github.com/cofi/dotfiles/blob/master/emacs.d/config/cofi-erc.el
      erc-track-exclude-types '("NICK" "JOIN" "LEAVE" "QUIT" "PART"
                                "301"   ; away notice
                                "305"   ; return from awayness
                                "306"   ; set awayness
                                ;;"324"   ; modes
                                ;;"329"   ; channel creation date
                                ;;"332"   ; topic notice
                                ;;"333"   ; who set the topic
                                ;;"353"   ; Names notice
                                ))

;; Truncate buffers so they don't hog core.
;;(add-hook 'erc-insert-post-hook 'erc-truncate-buffer)

;; timestamps - kstroem's from https://www.emacswiki.org/emacs/ErcStamp
(make-variable-buffer-local (defvar erc-last-datestamp nil))
(defun ks-timestamp (string)
  (erc-insert-timestamp-left string)
  (let ((datestamp (erc-format-timestamp (current-time) erc-datestamp-format)))
    (unless (string= datestamp erc-last-datestamp)
      (erc-insert-timestamp-left datestamp)
      (setq erc-last-datestamp datestamp))))

(setq erc-timestamp-only-if-changed-flag t
      erc-timestamp-format "%H:%M "
      erc-datestamp-format " === [%Y-%m-%d %a] ===\n" ; mandatory ascii art
      erc-fill-prefix "      "
      erc-insert-timestamp-function 'ks-timestamp)

;; this *must* not be installed before ERC is loaded
;; I'm currently do a hard `require' for 'erc above
;; but one never can be too careful.
(when (featurep 'erc)
  (defadvice save-buffers-kill-emacs (before save-logs (_arg) activate)
    "Save ERC buffers before killing Emacs."
    nil
    (save-some-buffers t (lambda ()
			   (when (and (eq major-mode 'erc-mode)
				      (not (null buffer-file-name))))))))


;; return a list of user names in the current buffer's channel
(defun my-get-channel-user-names ()
  "Return a list of the users in the ERC channel.

The current buffer must contain an ERC channel."
       (mapcar (lambda(x)
		 (vectorp (car (gethash x erc-channel-users))))
	       (hash-table-keys erc-channel-users)))

;; disable logging.
;; logging
(setq erc-log-insert-log-on-open t
      erc-log-channels t
      erc-log-channels-directory "~/.irclogs/"
      erc-save-buffer-on-part t
      erc-hide-timestamps nil
      erc-max-buffer-size 200000
      erc-truncate-buffer-on-save t
      )

;; (add-hook 'erc-insert-pqost-hook 'erc-save-buffer-in-logs)
;; (add-hook 'erc-mode-hook '(lambda () (when (not (featurep 'xemacs))
;; 				       (set (make-variable-buffer-local
;; 					     'coding-system-for-write)
;; 					    'emacs-mule))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;
;;; ALERT: author's devel work

;; ercn isn't built like other ERC modules so we can't roll it in, above.
(require 'ercn)

(if (eq system-type 'windows-nt)
    (progn
      (require 'erc-burnt-toast)
      (defun do-notify (nickname message)
	"Put notification in/on task-bar/notification center.

NICKNAME is the sender and MESSAGE is the content."
	(erc-burnt-toast-command "ERC@ema.cs" nickname message)))
  (message "todo: irc notification setup for non-windows systems"))

;; interactivly enable/disable alerting:
(defun os-notification-enable (&optional arg)
  "Enable (or, with ARG, disable) os/wm notifications."
  (interactive "P")
  (when (and (featurep 'ercn)
	     (fboundp 'do-notify))
    (funcall (if arg #'remove-hook #'add-hook)
	     'ercn-notify-hook #'do-notify)))

(defun auth-for (host)
  "Get the password for HOST."
  (let ((found (nth 0 (auth-source-search :host host))))
    (let ((secret (plist-get found :secret)))
      (if (functionp secret)
	  (funcall secret)
	secret))))

(defun twitch-irc ()
  "Connect to twitch IRC via twitch."
  (interactive)
  (erc-tls :server "irc.chat.twitch.tv"
           :port 6697
           :nick "mplsCorwin"
           :password (auth-for "twitch")))

(defun efnet-irc ()
  "Connect to Freenode IRC via Ghostwheel."
  (interactive)
  (erc-tls :server "ghostwheel.bru.st"
           :port 7062
           :nick "corwin"
           :password (format "corwin/ef:%s"
			     (auth-for "gw4-li"))))

(defun libera-irc ()
  "Connect to Libra IRC via Ghostwheel."
  (interactive)
  (erc-tls :server "ghostwheel.bru.st"
           :port 7062
           :nick "corwin"
           :password (format "corwin/libera:%s"
			     (auth-for "gw4-li"))))

(easy-menu-add-item  nil '("tools") ["IRC with ERC" my-irc t])

(defsubst my:erc-predicate (erc-server-buffer-or-name)
  "Return non-nil when ERC-SERVER-BUFFER-OR-NAME exists and is connected."
  (and erc-server-buffer-or-name
       (when-let ((buffer (if (bufferp erc-server-buffer-or-name)
			      erc-server-buffer-or-name
			    (get-buffer erc-server-buffer-or-name))))
	 (and (buffer-live-p buffer)
	      (erc-server-process-alive buffer)))))

(defun erc-connected-p (&optional buffer)
  "Check if BUFFER (or any buffer when BUFFER is nil) is connected."
  (interactive)
  (if buffer (my:erc-predicate buffer)
    (seq-find #'my:erc-predicate (buffer-list))))

;; (switch-to-buffer "ghostwheel.bru.st:7062")

 
;; just playing around; this does go here.
;;(use-package elcord :ensure t :config (elcord-mode))

;; TODO: really irritating to get notifications from every mention
;; after I reconnect to the bouncer so I'm manually adding the hook
;; after the connection is reestablished, for now.  What I *should* do
;; is: 1) create a function to add the hook on an idle timer after ERC
;; connects, and 2) remove the hook prior to reconnecting.
;; (add-hook 'ercn-notify-hook 'do-notify)

;; see os-notification-enable

;;;;
;; Windows 10 Only ERC Notifs: github.com/mplsCorwin/erc-burnt-toast
;; NOTE: I am not activity developing this while I focus on core and
;; cross-platform concerns.  I'll likely stop testing it myself soon.
;; If you are trying to use this and run into trouble please look at
;; w32-notification-notify and erc-match-text and erc-match-PRIVMSG
;; from erc-match, all available in current distributions of Emacs.
;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


;; this final bit of cruf is an unfinished experiment
;; creating a delayed reconnection feature that checks
;; for restoration of network after longer period of
;; inactivity, making some effort to

;; (defcustom my:erc-connect-functions
;;   '((#'freenode-irc "ghostwheel.bru.st:7062")
;;     (#'twitch-irc "irc.chat.twitch.tv:6697"))
;;   "Functions to connect paied with buffers to check for a connection."
;;   :type 'sexp :group 'erc) ;; punt on type and grroup

;; (defvar my:erc-delayed-reconnect-messages
;;   '(deferred reconnected)
;;   "When to emit messages related to delayed reconnection.")

;; (defvar my:erc-last-connected-when nil
;;   "A list of the last time we checked each connection.")

;; (defvar my:erc-delayed-reconnect-retry-minutes 10
;;   "Minutes to wait between checking for dead ERC sessions.")

;; (defvar my:erc-delayed-reconnect-minimum-seconds 600
;;   "Seconds to wait before first delayed reconnect attempt.

;; This may help to avoid join/part reconnection spam.  It could help
;; more to put this delay in between network join and subsiquent
;; staggered joins, especially if we flag larger channels.")

;; (defvar my:erc-connected-timer nil
;;   "While connected, a reconnection timer.

;; This helps to make sure we try reconnecting after longer network
;; outages which exhaust `erc-server-reconnect-count', e.g. spanning
;; beyond last actual time of `erc-server-reconnect-timeout'.")

;; ZZZ this was the point I realized we *need* to move to a DSL
;; I really don't want to build table after table against my
;; connection preferences to drive each function, nor do I
;; really want to count on having a single model across every
;; plugin/module or function I find or create.
;; (defun my:erc-delayed-reconnect (buffer-name)
;;   "Check BUFFER-NAME for an existing connection, otherwise
;;   reconnect if BUFFER-NAME exists, otherwise connect."
;;   (if (my:erc-predicate buffer-name)))

;;(defun my:erc-restart-reconnecting)

;; (setq erc-connected-timer
;; 	     (let ((erc-connect-args '(:server
;; 				 "chat.freenode.net"
;;                                  :etc "an so on"))
;; 	     (erc-connect-fun #'erc-tls)
;; 	     (erc-connected-repeat-seconds 90)
;; 	     erc-connected-timer)
;; 	       (or erc-connected-timer
;; 		   (setq erc-connected-timer
;; 			 (run-at-time (+ (float-time) erc-connected-repeat-seconds)
;; 				      erc-connected-repeat-seconds
;; 				      (lambda(&rest _)
;; 					(unless (seq-find #'erc-server-process-alive
;; 							  (buffer-list))
;; 					  (apply erc-connect-fun
;; 						 erc-connect-args))))))))

(provide 'init-erc)
;;; init-erc.el ends here
