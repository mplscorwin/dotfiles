;;; init-fun.el --- setup locally defined Emacs functions  -*- lexical-binding: t; -*-

;; Copyright (C) 2020  Corwin Brust

;; Author: Corwin Brust <corwin@bru.st>
;; Keywords:

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;;

;;; Code:

;; for finding non-asci chars
;; https://www.emacswiki.org/emacs/FindingNonAsciiCharacters
(defun occur-non-ascii ()
  "Find any non-ascii characters in the current buffer."
  (interactive)
  (occur "[^[:ascii:]]"))

;; (╯°□°)╯︵ ʞooqǝɔɐℲ
(defun insert-shrug (&optional arg)
  "Insert a shrug or, with ARG, something else."
  (interactive "P")
  (message "%s (%s)" arg (type-of arg))
  (insert (or (cond ((and (numberp arg) (> 0 arg)) "(┛ಠ_ಠ)┛彡┻━┻")
		    ((and arg (symbolp arg)) "┏━┓┏━┓┏━┓ ︵ /(^.^/)")
		    ((eq (car-safe arg) 16) "＼(≧▽≦)／")
		    (arg "(っಠ‿ಠ)っ"))
	      "¯\\_(ツ)_/¯")))

(global-set-key (kbd "C-x M-p") 'insert-shrug)

(defun my:slide-pagedown ()
  "Try to center on the next hunk within a slide.

Give `scroll-up-command' followed by `recenter-top-bottom'."
  (interactive)
  (scroll-up-command)
  (recenter-top-bottom))

(defun list-erc-server-buffers ()
  "List all `erc' server buffers."
  (interactive)
  ;;(message "ERC buffers: %s"  (mapconcat 'identity))
  (princ
   (list
    (seq-filter
     (lambda(b)
       (when (and (not (minibufferp b))
		  (erc-server-buffer-p b))
	 b))
     (buffer-list)))))

(defun get-hash-code (string)
  "Return STRING as an hash-code.")

(defun font-is-mono-p (font-family)
  ;; with-selected-window
  (let ((wind (selected-window))
        m-width l-width)
   (with-current-buffer "*Monospace Fonts*"
     (set-window-buffer (selected-window) (current-buffer))
     (text-scale-set 4)
     (insert (propertize "l l l l l" 'face `((:family ,font-family))))
     (goto-char (line-end-position))
     (setq l-width (car (posn-x-y (posn-at-point))))
     (newline)
     (forward-line)
     (insert (propertize "m m m m m" 'face `((:family ,font-family) italic)))
     (goto-char (line-end-position))
     (setq m-width (car (posn-x-y (posn-at-point))))
     (eq l-width m-width))))

(defun compare-monospace-fonts ()
  "Display a list of all monospace font faces."
  (interactive)
  (pop-to-buffer "*Monospace Fonts*")

  (erase-buffer)
  (dolist (font-family (font-family-list))
    (when (font-is-mono-p font-family)
      (let ((str font-family))
        (newline)
        (insert
         (propertize (concat "The quick brown fox jumps over the lazy dog 1 l; 0 O o ("
                             font-family ")\n") 'face `((:family ,font-family)))
         (propertize (concat "The quick brown fox jumps over the lazy dog 1 l; 0 O o ("
                             font-family ")\n") 'face `((:family ,font-family) italic)))))))

;; Friday, February 7th, 2020
(global-set-key (kbd "C-c d") 'insert-date)
(defun insert-date ()
  "Prompt for date using `org-read-date' then insert it."
  (interactive)
  (let* ((time (org-read-date nil t))
         (day (string-trim (format-time-string "%e" time)))
         (ordinal (diary-ordinal-suffix (string-to-number day))))
    ;;(message "%S" time)
    (insert (format-time-string (concat "%A, %B " day ordinal ", %Y")
                                time))))

;; https://github.com/rememberYou/.emacs.d/blob/master/config.org/
(use-package rainbow-delimiters
  :ensure t
  :hook (prog-mode . rainbow-delimiters-mode))

(defun my/smarter-move-beginning-of-line (arg)
  "Move point back to indentation of beginning of line.

   Move point to the first non-whitespace character on this line.
   If point is already there, move to the beginning of the line.
   Effectively toggle between the first non-whitespace character and
   the beginning of the line.

   If ARG is not nil or 1, move forward ARG - 1 lines first.  If
   point reaches the beginning or end of the buffer, stop there."
  (interactive "^p")
  (setq arg (or arg 1))

  ;; Move lines first
  (when (/= arg 1)
    (let ((line-move-visual nil))
      (forward-line (1- arg))))

  (let ((orig-point (point)))
    (back-to-indentation)
    (when (= orig-point (point))
      (move-beginning-of-line 1))))

(setq column-number-mode t)
;;(require 'autopair) ; required by init-editor

;;(require 'auto-complete-config)
;;(ac-config-default)

(defun untabify-buffer ()
  "This re-indents, untabifies, and cleans up whitespace.  It is stolen directly from the emacs-starter-kit."
  (interactive)
  (untabify (point-min) (point-max)))
(defun indent-buffer ()
  (interactive)
  (indent-region (point-min) (point-max)))
(defun cleanup-buffer ()
  "Perform a bunch of operations on the whitespace content of a buffer."
  (interactive)
  (indent-buffer)
  (untabify-buffer)
  (delete-trailing-whitespace))
(defun cleanup-region (beg end)
  "Remove tmux artifacts between BEG amd END."
  (interactive "r")
  (dolist (re '("\\\\|\·*\n" "\W*|\·*"))
    (replace-regexp re "" nil beg end)))
(setq-default show-trailing-whitespace t)


(use-package display-line-numbers
  :ensure nil ;;builtin
  :config
  (setq display-line-numbers-type 'relative)
  (display-line-numbers-mode -1))
;; I wrote this one ~corwin
(defun cycle-display-line-numbers ()
  "Cycle var `display-line-numbers' though nil, t and 'relative."
  (interactive)
  (setq display-line-numbers
	(if (eq 'relative display-line-numbers)
	    nil
	  (if display-line-numbers
	      'relative
	    t))))

 ;; Similar to M-q for fill, define M-Q for unfill
;; From:https://www.emacswiki.org/emacs/UnfillParagraph
(defun unfill-paragraph (&optional region)
  "Make single line of text from REGION or multi-line paragraph."
  (interactive (progn (barf-if-buffer-read-only) '(t)))
  (let ((fill-column (point-max))
        ;; This would override `fill-column' if it's an integer.
        (emacs-lisp-docstring-fill-column t))
    (fill-paragraph nil region)))

(defun yank-unfilled (&optional arg)
  "Unfill then yank."
  (interactive)
  (insert
   (save-excursion
     (with-temp-buffer
       (yank arg)
       (goto-char (point-min))
       (unfill-paragraph)
       (buffer-string)))))

(defun my:refill-paragraph (&optional region)
  "Unfill then re-fill multi-line paragraph or REGION."
  (interactive (progn (barf-if-buffer-read-only) '(t)))
  ;; (when-let ((unfill-func (or (local-key-binding "\M-q" t)
  ;; 			      (minor-mode-key-binding "\M-q" t)
  ;; 			      (global-key-binding "\M-q" t)))))
  (unfill-paragraph region)
  (paredit-reindent-defun region))

(provide 'init-fun)
;;; init-fun.el ends here
