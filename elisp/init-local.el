;;; init-local.el --- configure local environment dependancies  -*- lexical-binding: t; -*-

;; Copyright (C) 2020  Corwin Brust

;; Author: Corwin Brust <corwin@bru.st>
;; Keywords:

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;;

;;; Code:

;;(add-to-list 'load-path "c:/emacs/sitelisp/all-the-icons.el")

(when (eq system-type 'windows-nt)
  ;; remove if you plan to use AppData/Roaming/ as your emacs ~/
  (unless (getenv "Home")
    (setenv "Home" (getenv "UserProfile")))
  (add-to-list 'load-path "c:/emacs/sitelisp"))

;; (add-to-list 'load-path "~/site_lisp/emacs-status")

(add-to-list 'load-path "~/site_lisp")

(defconst user-init-dir
  (cond ((boundp 'user-emacs-directory)
         user-emacs-directory)
        ((boundp 'user-init-directory)
         user-init-directory)
        (t "~/.emacs.d/")))

;; fullframe for pdftools and magit
;;(require 'fullframe)

(provide 'init-local)
;;; init-local.el ends here
