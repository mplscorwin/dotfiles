;;; init-obs-command.el --- control obs with emacs           -*- lexical-binding: t; -*-

;; Copyright (C) 2021  Corwin Brust

;; Author: Corwin Brust <corwin@bru.st>
;; Keywords:

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;; control obs via shell-command
;; using OBSCommand for Windows
;; https://obsproject.com/forum/resources/command-line-tool-for-obs-websocket-plugin-windows.615/

;; OBSCommand.exe /server=127.0.0.1:4444 /password=xxxx /delay=0.5 /setdelay=0.05 /profile=myprofile /scene=myscene /hidesource=myscene/mysource /showsource=myscene/mysource /togglesource=myscene/mysource /toggleaudio=myaudio /mute=myaudio /unmute=myaudio /setvolume=mysource,volume,delay /fadeopacity=mysource,myfiltername,startopacity,endopacity,[fadedelay],[fadestep] /slidesetting=mysource,myfiltername,startvalue,endvalue,[slidedelay],[slidestep] /slideasync=mysource,myfiltername,startvalue,endvalue,[slidedelay],[slidestep] /startstream /stopstream /startrecording /stoprecording /command=mycommand,myparam1=myvalue1... /sendjson=jsonstring

;; this is a very simple attempt depending on OBSCommand
;; and only likely to work under windows, and then with
;; some care required, to be sure.

;;; Code:

;; didn't even take the time to make a macro here.
;; sheesh..

(defun my:show-emacs (&rest _) "Display more of Emacs."
	      (interactive)
	      (shell-command "c:\\obscommand_v1.5.4\\OBSCommand\\OBSCommand.exe /server=127.0.0.1:4444 /scene=miko-and-corwin:mor-emacs" "*Messages*"))
(defun my:show-minecraft (&rest _) "Display more of minecraft"
	      (interactive)
	      (shell-command "c:\\obscommand_v1.5.4\\OBSCommand\\OBSCommand.exe /server=127.0.0.1:4444 /scene=miko-and-corwin" "*Messages*"))
 (global-set-key (kbd "M-C-<f11>") #'my:show-emacs)
 (global-set-key (kbd "M-C-<f12>") #'my:show-minecraft)

(require 'subr-x)

(provide 'init-obs-command)
;;; init-obs-command.el ends here
