;;; init-obs.el --- control obs with emacs           -*- lexical-binding: t; -*-

;; Copyright (C) 2021  Corwin Brust

;; Author: Corwin Brust <corwin@bru.st>
;; Keywords:

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;; control obs via shell-command
;; using OBSCommand for Windows

;;; Code:

(require 'subr-x)

(defgroup my:obs nil "Settings for control OBS." :group 'local)

(defcustom my:obs-shell-command "C:\\OBSCommand_v1.5.4\\OBSCommand\\OBSCommand.exe"
  "Shell command to control OBS."
  :type 'string)

;; ZZZ it would be nice to complete a specific number of args
;; after completing a command.
;; TODO consider: keeping a count of placeholder
;; TODO consider: macro to populate into list, count placeholders?
(defcustom my:obs-command-alist '((scene "/scene=%s"))
  "Formats for injecting args into `my:obs-shell-command."
  :type '(aplist :value-type string))

(defcustom my:obs-scene-alist '("lower-left"
				"lower-right"
				"upper-left"
				"upper-right"
				"giant-corwin")
  "A list of OBS scenes we can switch to."
  :type '(repeat string))

(defcustom my:obs-buffer-name "*obs command log*"
  "Name of a buffer to be used to store the obs log, or nil to suppress.")

(defvar my:obs-command-hist nil "History of OBS commands.")

(defun my:obs-command (command &rest args)
  "Execute an OBS COMMAND with ARGS.

COMMAND is a symbol and ARGS are replacements for the format
assocated in `my:obs-command-plist'.  Raise an error when COMMAND
is nil or not present in `my:obs-command-plist'."
  (interactive (append (list
			(completing-read "OBS Command:"
					 (mapcar 'car my:obs-command-alist)
					 nil t nil my:obs-command-hist
					 (car-safe my:obs-command-hist)))
		     (completing-read-multiple "Args (comma seperated):" nil)))
  ;;(message "command:%s args[%s]" command args)
  (unless command (user-error "No command"))
  (if-let* ((command (if (stringp command) (intern command)
		      command))
	    (fmt (car-safe
		    (cdr-safe (assoc command
				     my:obs-command-alist))))
	    (formatted-command
	     (apply #'format (append
			      (list
			       (mapconcat 'identity (list my:obs-shell-command fmt) " "))
			      args))))
      (progn (when (not (null my:obs-buffer-name))
	       (let ((buf (get-buffer my:obs-buffer-name)))
		 (with-current-buffer (or buf (get-buffer-create my:obs-buffer-name))
		   ;; (when buf (goto-char (point-max)))
		   (insert (format "%sSending command: %s\nResults: "
				   (if buf "\n" "")
				   formatted-command)))))
	     (shell-command formatted-command my:obs-buffer-name my:obs-buffer-name))
    (user-error "Unknown command" command)))

;;(shell-command "C:\\OBSCommand_v1.5.4\\OBSCommand\\OBSCommand.exe /scene=lower-right")


(provide 'init-obs)
;;; init-obs.el ends here
