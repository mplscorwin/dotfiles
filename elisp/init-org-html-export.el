;; This buffer is for text that is not saved, and for Lisp evaluation.
;; To create a file, visit it with C-x C-f and enter text in its buffer.

(remove-hook 'org-mode-hook 'org-html-themify-mode)

(setq org-html-themify-themes
      '((dark . modus-vivendi)
        (light . tao-yang)
	(funky . cyberpunk-2019)))

;; ZZZ working around an error
(unless (and (bound-and-true-p hl-line) (facep 'hl-line))
  (defface hl-line nil "stub for hl-line" :group 'my))

(add-hook 'org-mode-hook 'org-html-themify-mode)
