;;; init-org.el --- configure org-mode  -*- lexical-binding: t; -*-

;; Copyright (C) 2020  Corwin Brust

;; Author: Corwin Brust <corwin@bru.st>
;; Keywords:

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;; global configuration ment for Emacs startup sequence

;;; Code:

(eval-when-compile (require 'org)
		   ;;(require 'org-agenda)
		   )

;; org mode
(setq org-log-done t
      org-hide-leading-stars t
      org-hide-emphasis-markers t
      org-todo-keywords '((sequence "TODO" "INPROGRESS" "DONE"))
      org-todo-keyword-faces '(("INPROGRESS" . (:foreground "blue" :weight bold))))
(add-hook 'org-mode-hook
          (lambda ()
            (flyspell-mode)))
(add-hook 'org-mode-hook
          (lambda ()
            (writegood-mode)))
(setq org-agenda-show-log t
      org-agenda-todo-ignore-scheduled t
      org-agenda-todo-ignore-deadlines t)
(setq org-agenda-files "~/org-agenda-files.el")

(require 'org)
(require 'org-agenda)

(use-package org-agenda-property :ensure t
  :config (setq org-agenda-property-list '("LOCATION" "Responsible")))

;; org category setup
(setq org-agenda-category-icon-alist
      `(("Family"      ,(concat org-directory "/images/family.png") nil nil :ascent center)
	("Cleaning"      ,(concat org-directory "/images/broom.png") nil nil :ascent center)
	("Social"      ,(concat org-directory "/images/social.png") nil nil :ascent center)
        ("Dungeon"       ,(concat org-directory "/images/dm-1d6-32.png") nil nil :ascent center)
        ("Emacs"         ,(concat org-directory "/images/emacs-sm.png") nil nil :ascent center)))

;;(require 'org-install)

(require 'org-habit)
(add-to-list 'org-modules 'org-habit)

;;(setq org-modules '(ol-w3m ol-bbdb ol-bibtex ol-docview ol-gnus ol-info ol-irc ol-mhe ol-rmail ol-eww))
;;(add-to-list 'org-modules "org-habit")
;;(message "m:%s" org-modules)

(setq org-habit-preceding-days 7
      org-habit-following-days 1
      org-habit-graph-column 80
      org-habit-show-habits-only-for-today t
      org-habit-show-all-today t)

;; org babel
(require 'ob)
(org-babel-do-load-languages
 'org-babel-load-languages
 '((C     . t)
   (ditaa . t)
   (dot   . t)
   ;; (http  . t) ; local source
   (emacs-lisp . t)
   (latex . t)
   ;(ruby . t)
   (js    . t)
   (perl  . t)
   (plantuml . t)
   (shell . t)
   ;; (typescript . t) ; not always loaded
   ))

(add-to-list 'org-src-lang-modes (quote ("dot". graphviz-dot)))
(add-to-list 'org-src-lang-modes (quote ("plantuml" . fundamental)))
(add-to-list 'org-babel-tangle-lang-exts '("clojure" . "clj"))
(defvar org-babel-default-header-args:clojure
  '((:results . "silent") (:tangle . "yes")))
(defun org-babel-execute:clojure (body params)
  "Docstring using BODY and PARAMS."
  (lisp-eval-string body) "Done!")
;(provide 'ob-clojure)
(setq org-src-fontify-natively t
      org-confirm-babel-evaluate nil)
(add-hook 'org-babel-after-execute-hook
	  (lambda ()
	    (condition-case nil
		(org-display-inline-images)
	      (error nil)))
          'append)
(add-hook 'org-mode-hook (lambda () (abbrev-mode 1)))

;;; ORG visual setup
;;(load-theme 'org-beautify)
;;(use-package org-bullets)
;;(add-hook 'org-mode-hook (lambda () (org-bullets-mode 1)))

;; plantuml - http://plantuml.com/
(setq org-plantuml-jar-path "c:/emacs/bin/plantuml.jar")

;;;;;;;;;; setup org-expiry and org-capture: https://github.com/mrvdb/emacs-config
(use-package org-expiry
  :ensure nil
  ;; :load-path (lambda () (concat custom-package-directory "org-expiry/"))
  :init
  (setq org-expiry-created-property-name "CREATED")
  (setq org-expiry-inactive-timestamps   t))

(defun mrb/insert-created-timestamp()
  "Insert a CREATED property using org-expiry.el for TODO entries."
  (org-expiry-insert-created)
  (org-back-to-heading)
  (org-end-of-line)
)

(defadvice org-insert-todo-heading (after mrb/created-timestamp-advice activate)
  "Insert a CREATED property using org-expiry.el for TODO entries."
  (mrb/insert-created-timestamp))

(ad-activate 'org-insert-todo-heading)
;;(ad-deactivate 'org-insert-todo-heading)

(use-package org-capture :ensure nil)
(defadvice org-capture (after mrb/created-timestamp-advice activate)
  "Insert a CREATED property using org-expiry.el for TODO entries."
  (when (member (org-get-todo-state) org-todo-keywords-1)
    (mrb/insert-created-timestamp)))
(ad-activate 'org-capture)
;;(ad-deactivate 'org-capture)

(defun mrb/org-schedule-for-today()
  "Schedule the current item for today."
  (interactive)
  (org-schedule nil (format-time-string "%Y-%m-%d")))

(defun mrb/org-agenda-schedule-for-today()
  "Schedule the current item in the agenda for today."
  (interactive)
  (org-agenda-schedule nil (format-time-string "%Y-%m-%d")))

;;;;;;;;;;  end org-expire setup

(provide 'init-org)
;;; init-org.el ends here
