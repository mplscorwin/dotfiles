;;; init-package-managment.el --- setup Emacs package management  -*- lexical-binding: t; -*-

;; Copyright (C) 2020  Corwin Brust

;; Author: Corwin Brust <corwin@bru.st>
;; Keywords:

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;;

;;; Code:

;; bootstrap use-package
(unless (require 'use-package nil t)
  ;; make sure MELPA is avialble
  (require 'package)
  (add-to-list 'package-archives '("MELPA" . "https://melpa.org/packages/") t)
  ;; then install use-package the "hard" way
  (package-refresh-contents)
  (package-install 'use-package))

;; initalize use-package, base of our package management approach
(use-package package :ensure nil
  :config
  (add-to-list 'package-archives '("MELPA" . "https://melpa.org/packages/") t)
  (when (version< emacs-version "27.0")
    (package-initialize)))

;; further bootstrapping for native-comp
;; https://gist.github.com/kiennq/cfe57671bab3300d3ed849a7cbf2927c
(use-package async
  :ensure t
  :defer 5
  :init
  (setq async-bytecomp-allowed-packages '(all))
  :config
  ;; async compiling package
  (async-bytecomp-package-mode t)
  (dired-async-mode 1)
  ;; limit number of async processes
  (my-require-cl)
  (defvar async-maximum-parallel-procs 4)
  (defvar async--parallel-procs 0)
  (defvar async--queue nil)
  (defvar-local async--cb nil)
  (advice-add #'async-start :around
              (lambda (orig-func func &optional callback)
                (if (>= async--parallel-procs async-maximum-parallel-procs)
                    (push `(,func ,callback) async--queue)
                  (cl-incf async--parallel-procs)
                  (let ((future (funcall orig-func func
                                         (lambda (re)
                                           (cl-decf async--parallel-procs)
                                           (when async--cb (funcall async--cb re))
                                           (when-let (args (pop async--queue))
                                             (apply #'async-start args))))))
                    (with-current-buffer (process-buffer future)
                      (setq async--cb callback)))))
              '((name . --queue-dispatch))))

;; should be setup by early-init for >= 27.1
(unless (featurep 'auto-compile)
  (use-package auto-compile :ensure t
    :init (setq load-prefer-newer t)
    :config
    (auto-compile-on-load-mode)
    (auto-compile-on-save-mode)))

;; https://github.com/wyuenho/dotfiles/blob/master/.emacs
;; Stop asking me if a theme is safe. The entirety of Emacs is built around
;; evaling arbitrary code...
(advice-add 'load-theme :around (lambda (old-load-theme &rest args)
                                  "Don't ask for confirmation when loading a theme."
                                  (apply old-load-theme (car args) t (cddr args))))


(use-package use-package :ensure t)
(use-package diminish :ensure t)

(provide 'init-package-managment)
;;; init-package-managment.el ends here
