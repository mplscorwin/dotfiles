;;; init-perl.el --- initalize Perl related Emacs customization  -*- lexical-binding: t; -*-

;; Copyright (C) 2020  Corwin Brust

;; Author: Corwin Brust <corwin@bru.st>
;; Keywords: perl

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;; Setup perltidy
;; TODO: move company stuff for perl in here

;; Lifted whole cloth from Robert Boone, with thanks.
;; https://gist.github.com/rlb3/128694/0c32aa352248ccddcdbefb4e3a0c7366533bb0f4

;;; Code:
;; perltidy region with <strg><c> <t>
(defmacro mark-active ()
  "Xemacs/emacs compatibility macro"
  (if (boundp 'mark-active)
      'mark-active
    '(mark)))

(defun perltidy ()
  "execute perltidy for the selected region or the current buffer"
  (interactive)
  ; save-excursion doesn't work somehow... so:
  (let ((orig-point (point)))
    (unless (mark-active) (mark-defun))
    (shell-command-on-region (point) (mark) "perltidy -q" nil t)
    (goto-char orig-point)))

(global-set-key "\C-ct" 'perltidy)

(defun perl-tidy-full ()
  (interactive)
  (save-excursion
    (mark-whole-buffer)
    (perltidy)))

(global-set-key "\C-c T" 'perl-tidy-full)


(provide 'init-perl)
;;; init-perl.el ends here
