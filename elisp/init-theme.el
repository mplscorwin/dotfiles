;;; init-theme.el --- Emacs visual setup             -*- lexical-binding: t; -*-

;; Copyright (C) 2020  Corwin Brust

;; Author: Corwin Brust <corwin@bru.st>
;; Keywords:

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;;

;;; Code:

(eval-when-compile (require 'use-package))

(use-package all-the-icons
  :ensure t
  :if (display-graphic-p)
  :config
  (add-hook 'after-change-major-mode-hook
            (lambda ()
              (let* ((icon (all-the-icons-icon-for-mode major-mode))
                     (face-prop (and (stringp icon)
				     (get-text-property 0 'face icon))))
                (when (and (stringp icon) (not (string= major-mode icon)) face-prop)
                  (setq mode-name (propertize icon 'display '(:ascent center)))))))
  (defface all-the-icons-white
    '((((background dark)) :foreground "#AA759F")
      (((background light)) :foreground "#68295B"))
    "Face for white icons"
    :group 'all-the-icons-faces)
  (defface all-the-icons-white-alt
    '((((background dark)) :foreground "#5D54E1")
      (((background light)) :foreground "#5D54E1"))
    "Face for white icons"
    :group 'all-the-icons-faces)
  (defface all-the-icons-lwhite
    '((((background dark)) :foreground "#E69DD6")
      (((background light)) :foreground "#E69DD6"))
    "Face for lwhite icons"
    :group 'all-the-icons-faces)
  (defface all-the-icons-dwhite
    '((((background dark)) :foreground "#694863")
      (((background light)) :foreground "#694863"))
    "Face for dwhite icons"
    :group 'all-the-icons-faces))

;; (require 'doom-themes)
;; (load-theme 'doom-one)
;; (doom-themes-treemacs-config)

(use-package doom-themes
  ;;:disabled t
   :after treemacs all-the-icons
  :ensure t
  ;;  :init (load-theme 'doom-one t)
  :config
  ;; Enable flashing mode-line on errors
  (doom-themes-visual-bell-config)
  ;; Corrects (and improves) org-mode's native fontification.
  (doom-themes-org-config)
  ;; Enable custom treemacs theme (all-the-icons must be installed!)
  (when (featurep 'all-the-icons)
    (doom-themes-treemacs-config)))

(use-package powerline :ensure t
  :config (powerline-default-theme))
(use-package cyberpunk-theme :ensure t
  :config (load-theme 'cyberpunk t))
(eval-after-load 'erc
  (use-package cyberpunk-2019-theme :ensure t
    :config (load-theme 'cyberpunk-2019 t)))

(defun my:after-make-frame (frame)
  "Function to run after creating FRAME."
  (with-selected-frame frame
    (when (display-graphic-p frame)
      (set-frame-parameter frame 'alpha '(90 . 65)))))

(set-frame-parameter (selected-frame) 'alpha '(90 . 65))
(add-hook 'after-make-frame-functions #'my:after-make-frame)
;;(add-to-list 'initial-frame-alist '(alpha (90 . 65)))
;;(add-to-list 'default-frame-alist '(alpha (90 . 65)))

(provide 'init-theme)
;;; init-theme.el ends here
