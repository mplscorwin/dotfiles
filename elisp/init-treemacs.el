;;; init-treemacs.el --- setup Treemacs navigation sidecar  -*- lexical-binding: t; -*-

;; Copyright (C) 2020

;; Author:  <corwi@AVALON>
;; Keywords:

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;;

;;; Code:


;; Treemacs
(use-package treemacs
  :ensure t
  :defer t
  :init
  (with-eval-after-load 'winum
    (define-key winum-keymap (kbd "M-0") #'treemacs-select-window))
  :bind
  (:map global-map
	([f8]        . treemacs)
        ("M-0"       . treemacs-select-window)
        ("C-x t 1"   . treemacs-delete-other-windows)
        ("C-x t t"   . treemacs)
        ("C-x t B"   . treemacs-bookmark)
        ("C-x t C-t" . treemacs-find-file)
        ("C-x t M-t" . treemacs-find-tag))
  :config
  ;;(global-set-key [f8] 'treemacs)
  (progn
    ;;(treemacs-follow-mode t)
    ;;(treemacs-filewatch-mode t)
    (treemacs-fringe-indicator-mode t)
    ;; (pcase (cons (not (null (executable-find "git")))
    ;;              (not (null treemacs-python-executable)))
    ;;   (`(t . t)  (treemacs-git-mode 'deferred))
    ;;   (`(t . _)  (treemacs-git-mode 'simple)))
    )
  ;; (setq treemacs-width 35
  ;;       treemacs-display-in-side-window t
  ;;       treemacs-indentation-string (propertize " " 'face 'font-lock-comment-face)
  ;;       treemacs-indentation 1)
  ;; ;;(add-hook 'treemacs-mode-hook #'hide-mode-line-mode)
  ;; (add-hook 'treemacs-mode-hook (lambda ()
  ;;                                 (linum-mode -1)
  ;;                                 (fringe-mode 0)
  ;;                                 (setq buffer-face-mode-face `(:background "#211C1C"))
  ;;                                 (buffer-face-mode 1)))
  ;; ;; ;; Improve treemacs icons
  ;; (with-eval-after-load 'treemacs
  ;;   (with-eval-after-load 'all-the-icons
  ;;     (let ((all-the-icons-default-adjust 0)
  ;; 	    (tab-width 1))
  ;; 	;; Root icon
  ;; 	(setq treemacs-icon-root-png
  ;; 	      (concat (all-the-icons-octicon "repo" :height 0.8 :v-adjust -0.2)  " "))
  ;;       ;; File icons
  ;;       (setq qtreemacs-icon-open-png
  ;;             (concat
  ;;              (all-the-icons-octicon "chevron-down" :height 0.8 :v-adjust 0.1)
  ;;              "\t"
  ;;              (all-the-icons-octicon "file-directory" :v-adjust 0)
  ;;              "\t")
  ;;             treemacs-icon-closed-png
  ;;             (concat
  ;;              (all-the-icons-octicon "chevron-right" :height 0.8
  ;;                                     :v-adjust 0.1 :face 'font-lock-doc-face)
  ;;              "\t"
  ;;              (all-the-icons-octicon "file-directory" :v-adjust 0 :face 'font-lock-doc-face)
  ;;              "\t"))
  ;;       ;; File type icons
  ;;       (setq treemacs-icons-hash (make-hash-table :size 200 :test #'equal)
  ;;             treemacs-icon-fallback (concat
  ;;                                     "\t\t"
  ;;                                     (all-the-icons-faicon "file-o" :face 'all-the-icons-dsilver
  ;;                                                           :height 0.8 :v-adjust 0.0)
  ;;                                     "\t")
  ;;             treemacs-icon-text treemacs-icon-fallback)
  ;;       (dolist (item all-the-icons-icon-alist)
  ;;         (let* ((extension (car item))
  ;;                (func (cadr item))
  ;;                (args (append (list (caddr item)) '(:v-adjust -0.05) (cdddr item)))
  ;;                (icon (apply func args))
  ;;                (key (s-replace-all '(("^" . "") ("\\" . "") ("$" . "") ("." . "")) extension))
  ;;                (value (concat "\t\t" icon "\t")))
  ;;           (unless (ht-get treemacs-icons-hash (s-replace-regexp "\\?" "" key))
  ;;             (ht-set! treemacs-icons-hash (s-replace-regexp "\\?" "" key) value))
  ;;           (unless (ht-get treemacs-icons-hash (s-replace-regexp ".\\?" "" key))
  ;; 	      (ht-set! treemacs-icons-hash (s-replace-regexp ".\\?" "" key) value)))))))
  )

(use-package treemacs-projectile
  :after treemacs projectile
  :ensure t)

(use-package treemacs-icons-dired
  :after treemacs dired
  :ensure t
  :config (treemacs-icons-dired-mode))

;; (use-package treemacs-magit
;;   :after treemacs magit
;;   :ensure t)


(provide 'init-treemacs)
;;; init-treemacs.el ends here
