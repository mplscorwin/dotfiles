;;; init-vc.el --- setup Emacs version control support  -*- lexical-binding: t; -*-

;; Copyright (C) 2020  Corwin Brust

;; Author: Corwin Brust <corwin@bru.st>
;; Keywords:

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;;

;;; Code:

(use-package magit
  :after (fullframe)
  :bind
  ("C-c m" . magit-status)
  :init
  (fullframe magit-status magit-mode-quit-window)
  :config
  (setq magit-last-seen-setup-instructions "1.4.0"))

;; not working under 28 due to SQLite related issues
;; NOTE: other things that use sqllite work and SQLite binaries (and DLL) are on path
;; (use-package forge
;;   :ensure t
;;   :after magit
;;   :config
;;   (setq forge-add-pullreq-refspec 'ask
;;         forge-pull-notifications t
;;         forge-topic-list-limit '(60 . 0)))

(provide 'init-vc)
;;; init-vc.el ends here
