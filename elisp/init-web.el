;;; init-web.el --- configure Emacs for web development  -*- lexical-binding: t; -*-

;; Copyright (C) 2020

;; Author:  <corwi@AVALON>
;; Keywords:

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;;

;;; Code:

;; cperl mode
(defalias 'perl-mode 'cperl-mode)
(setq cperl-hairy t)
(use-package plsense
  :ensure t
  :config  ;; direct fm https://github.com/aki2o/emacs-plsense
  ;;(setq plsense-popup-help-key "C-:")
  ;;(setq plsense-display-help-buffer-key "M-:")
  ;;(setq plsense-jump-to-definition-key "C->")
  ;; Make config suit for you. Eval the following sexp:
  ;; ;; (customize-group "plsense")
  ;; Recommemded configuration:
  (plsense-config-default))


;; rely on flycheck, typescript confuses js2/rjsx ~corwin
;(setq js2-strict-missing-semi-warning nil)
;(setq js2-missing-semi-one-line-override t)
;(setq js2-strict-missing-semi-warning nil)
;(setq js2-mode-show-strict-warnings nil)
;(setq js2-mode-show-parse-errors nil)

;; using devel js2-mode for TS generics, etc
;; https://github.com/HairyRabbit/js2-mode/issues/6
;; https://github.com/HairyRabbit/js2-mode
(load-library "~/site_lisp/js2-mode.elc")
(load-library "~/site_lisp/js2-imenu-extras.elc")

(defun setup-tide-mode ()
  (interactive)
  (tide-setup)
  (flycheck-mode +1)
  (setq flycheck-check-syntax-automatically '(save mode-enabled))
  (eldoc-mode +1)
  (tide-hl-identifier-mode +1)
  ;; company is an optional dependency. You have to
  ;; install it separately via package-install
  ;; `M-x package-install [ret] company`
  (when (featurep 'company) (company-mode +1)))


(use-package company-quickhelp
  :ensure t
  :init
  (company-quickhelp-mode 1)
  (use-package pos-tip
    :ensure t))

;; (use-package company-box
;;   :diminish
;;   :hook (company-mode . company-box-mode))

(use-package web-mode
  :ensure t
  :mode (("\\.html?\\'" . web-mode)
         ;("\\.tsx\\'" . web-mode)
	 ;("\\.jsx\\'" . web-mode)
	 )
  :config
  (setq web-mode-markup-indent-offset 2
        web-mode-css-indent-offset 2
        web-mode-code-indent-offset 2
        web-mode-block-padding 2
        web-mode-comment-style 2

        web-mode-enable-css-colorization t
        web-mode-enable-auto-pairing t
        web-mode-enable-comment-keywords t
        web-mode-enable-current-element-highlight t
        )
  (add-hook 'web-mode-hook
            (lambda ()
              (when (string-equal "tsx" (file-name-extension buffer-file-name))
		(setup-tide-mode))))
  ;; enable typescript-tslint checker
  (flycheck-add-mode 'typescript-tslint 'web-mode))

(use-package rjsx-mode
  :ensure t
  :mode (
	 ("\\.js\\'"  . rjsx-mode)
	 ("\\.jsx\\'" . rjsx-mode)
	 ("\\.tsx\\'" . rjsx-mode)))

(use-package typescript-mode
  :ensure t
  :config
  (setq typescript-indent-level 2)
  (add-hook 'typescript-mode #'subword-mode))

(use-package tide
  :init
  :ensure t
  :after (typescript-mode company flycheck)
  :hook ((typescript-mode . tide-setup)
         (typescript-mode . tide-hl-identifier-mode)
	;(before-save . tide-format-before-save)
	 ))

;;https://gist.github.com/CodyReichert/9dbc8bd2a104780b64891d8736682cea
;; use eslint with rjsx-mode for jsx files
(flycheck-add-mode 'javascript-eslint 'rjsx-mode)
;; Enable eslint checker for web-mode
(flycheck-add-mode 'javascript-eslint 'web-mode)
;; Enable flycheck globally
(add-hook 'after-init-hook #'global-flycheck-mode)

(add-hook 'rjsx-mode-hook
	  (lambda ()
	    (when (string-equal "tsx" (file-name-extension buffer-file-name))
	      (setup-tide-mode))))

(provide 'init-web)
;;; init-web.el ends here
