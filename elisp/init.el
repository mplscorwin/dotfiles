;;; init.el --- min startup script for Emacs        -*- lexical-binding: t; -*-

;; Copyright (C) 2020

;; Author:  <corwi@AVALON>
;; Keywords:

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;; TODO  add a var to suppress loading e.g. via -Q --eval "(setq ..

;;; Code:

;; we'll need cl/cl-lib but we hate errors
;; also, cl-pushnew seems to have dissapeared from cl's autoloads at some point
(defmacro my-require-cl nil
  "Require cl under 26/27/28 without warnings."
  `(if (version< emacs-version "26.0")
       (eval-when-compile
	 (require 'cl))
     (require 'cl-lib)))

(my-require-cl)

;; (define-inline my:init-directory (&optional dir) ;;
;;   "Assume the current (or DIR) is where Emacs init scripts are kept."
;;   (or dir (file-name-directory (buffer-file-name))))
;; (add-to-list 'load-path (my:init-directory))

(unless (bound-and-true-p elisp-dotfiles)
  (defconst elisp-dotfiles
    (if (eq system-type 'windows-nt)
	"d:/projects/dotfiles/elisp"
      "~/dotfiles/elisp")
    "Location for personal init scripts."))
;;(setq elisp-dotfiles "d:/projects/dotfiles/elisp/")
(cl-pushnew elisp-dotfiles load-path)
(cl-pushnew (format "%s/%s" elisp-dotfiles "site-lisp") load-path)

(defvar my:init-files nil
  "Init-files as an hash-table.

Keys are as for completion, values are original filenames.")

(defun my:init-basename (file)
  "Return FILE as for completion.

Remove leading \"init-\" and trailing \".elc?\"."
  (let ((short (or (and (< 5 (length file))
			 (string-match-p "^init-" file)
			 (substring file 5))
		    file)))
    (save-match-data
      (if (string-match "^\\(.*\\)[.]elc?$" short)
	  (match-string 1 short)
	short))))

(defun my:init-files (files)
  "Populate HASH-TABLE from FILES.

Return the sorted list of table keys."
  (setq my:init-files (make-hash-table :size (length files)))
  (let (short-name)
    (sort (mapcar
	   (lambda (file)
	     (prog1 (setq short-name (my:init-basename file))
	       (puthash file short-name my:init-files)))
	   files)
	  #'my:compare-el-files)))

(defun my:sort-el-files (a b)
  "Sort A and B, members of mixed list of .el and .elc files.

Prefer .elc files, meaning they should come first."
  (let ((ba (file-name-base a))
        (bb (file-name-base b)))
    (or (string> ba bb)
        (and (string= ba bb)
             (string-match-p ".elc$" a)))))

(defun my:compare-el-files (a b)
  "Return nil unless A and B are the same file.

For this purpose, .el and .elc files are the same."
  (string= (file-name-base a)
	   (file-name-base b)))

(defun my:load-init-library (&optional pkg)
  "Load PKG, an init-script."
  (interactive ;; load-library@1041c:/emacs26.3/share/emacs/26.3/lisp/files.el
   (let ((scripts (seq-uniq (my:init-files
			     (seq-filter
			      (apply-partially 'string-match-p "^init-.*[.]elc?$")
			      (directory-files elisp-dotfiles)))
			    #'my:sort-el-files
			    )))
     (list (format "init-%s" (completing-read "Load init: "
					      scripts
					      nil
					      t)))))
  (when pkg (load pkg)))

;; don't put customizations into .emacs
(setq custom-file (concat elisp-dotfiles "init-customize.el"))

(dolist (pkg '(init-local
	       init-package-managment
	       init-editor
	       init-org
	       init-elisp
	       init-projectile
	       init-vc
	       ;;init-company
	       init-fun
	       init-bindkey
	       init-theme
	       ))
  (require pkg))

;; load customizations after packages
;; so we can customize packages
(load custom-file)

(provide 'init)
;;; init.el ends here
