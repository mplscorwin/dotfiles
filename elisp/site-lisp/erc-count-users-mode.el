;;; erc-countusers-mode.el --- display a count of channel users in the mode-string
;;; Commentary:
;;    TODO: return empty string if parted/disconnected
;;    TODO: add to emacswiki files
;;; Code:
(define-minor-mode erc-count-users-mode "https://www.emacswiki.org/emacs/ErcModeline

Add this to your .emacs to see the number of opped/voiced/normal members of the
current channel in the modeline:

  (require 'erc-count-users-mode)
  (add-hook 'erc-join-hook #'erc-count-users-mode)" nil
      (:eval
       (let ((ops 0)
             (voices 0)
             (members 0))
         (maphash (lambda (key value)
                    (when (erc-channel-user-op-p key)
                      (setq ops (1+ ops)))
                    (when (erc-channel-user-voice-p key)
                      (setq voices (1+ voices)))
                    (setq members (1+ members)))
                  erc-channel-users)
         (format " %S/%S/%S" ops voices members))))
(provide 'erc-count-users-mode)
;;; erc-count-users-mode.el ends here
