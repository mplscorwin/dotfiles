#!/bin/env bash
# find local sources for packages required by el files in cwd
skip="org|erc|use"
strip="print \$1 if /([\w-]+)\)/ and \$1 !~/^\(?:$skip\)$/"
dirs="/c/emacs/sitelisp /c/users/corwi/site_lisp"
for p in $( grep require *.el | perl -nle "$strip" ) ;
do
    for d in $dirs ;
    do
	ls -l $d/${p}.el 2>/dev/null ;
    done ;
done
